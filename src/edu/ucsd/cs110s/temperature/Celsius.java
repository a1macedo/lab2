/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author a1macedo
 *
 */
public class Celsius extends Temperature{

	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return String.valueOf(this.getValue());
	}
	@Override
	public Temperature toCelsius() {
		//get value of input temperature		
		return new Celsius(this.getValue());
	}
	@Override
	public Temperature toFahrenheit() {
		//get Celsius value
		float nTemp = super.getValue();
		nTemp = (float)(nTemp * (1.8));
		nTemp = nTemp + 32;
		
		//return new temperature
		return new Fahrenheit(nTemp);
	}
}
