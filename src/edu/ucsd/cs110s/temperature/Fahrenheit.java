/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author a1macedo
 *
 */
public class Fahrenheit extends Temperature{

	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return String.valueOf(this.getValue());
	}
	@Override
	public Temperature toCelsius() {
		float nTemp = super.getValue();
		nTemp = nTemp - 32;
		nTemp = (float)(nTemp * (.555));
		
		//return new temperature
		return new Celsius(nTemp);
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit(this.getValue());
	}
}
